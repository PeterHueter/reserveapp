<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_photos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('restaurant_id');
            $table->string('link');
            $table->boolean('cover');

            $table->timestamps();

            $table->index('restaurant_id');

            $table->foreign('restaurant_id', 'photos_restaurant_id_foreign')
                ->references('id')
                ->on('restaurants')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('restaurant_photos', function (Blueprint $table) {
            $table->dropForeign('photos_restaurant_id_foreign');
            $table->drop();
        });
    }
}
