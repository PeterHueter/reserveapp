<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantPurposesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_restaurant_purposes', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->unsignedInteger('restaurant_id');
            $table->unsignedInteger('purpose_id');
            $table->primary(['restaurant_id', 'purpose_id']);

            $table->foreign('restaurant_id', 'restaurant_purposes_id_foreign')
                ->references('id')
                ->on('restaurants')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('purpose_id', 'purposes_restaurant_id_foreign')
                ->references('id')
                ->on('purposes')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('restaurant_restaurant_purposes', function (Blueprint $table) {
            $table->dropForeign('restaurant_purposes_id_foreign');
            $table->dropForeign('purposes_restaurant_id_foreign');
            $table->drop();
        });
    }
}
