<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurants', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->text('description');
            $table->unsignedTinyInteger('rating');
            $table->boolean('available');
            $table->integer('min_price');
            $table->string('street', 100);
            $table->decimal('lat', 9, 6);
            $table->decimal('long', 9, 6);
            $table->time('monday_to');
            $table->time('monday_from');
            $table->time('tuesday_to');
            $table->time('tuesday_from');
            $table->time('wednesday_to');
            $table->time('wednesday_from');
            $table->time('thursday_to');
            $table->time('thursday_from');
            $table->time('friday_to');
            $table->time('friday_from');
            $table->time('saturday_to');
            $table->time('saturday_from');
            $table->time('sunday_to');
            $table->time('sunday_from');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurants');
    }
}
