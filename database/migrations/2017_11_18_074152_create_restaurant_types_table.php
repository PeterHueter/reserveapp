<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('restaurant_restaurant_types', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->unsignedInteger('restaurant_id');
            $table->unsignedInteger('type_id');
            $table->primary(['restaurant_id', 'type_id']);

            $table->foreign('restaurant_id', 'restaurant_type_id_foreign')
                ->references('id')
                ->on('restaurants')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('type_id', 'type_restaurant_id_foreign')
                ->references('id')
                ->on('restaurant_types')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('restaurant_restaurant_types', function (Blueprint $table) {
            $table->dropForeign('restaurant_type_id_foreign');
            $table->dropForeign('type_restaurant_id_foreign');
            $table->drop();
        });

        Schema::table('restaurant_types', function (Blueprint $table) {
            $table->drop();
        });
    }
}
