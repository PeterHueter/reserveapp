<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantFeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_features', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('restaurant_restaurant_features', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->unsignedInteger('restaurant_id');
            $table->unsignedInteger('feature_id');
            $table->primary(['restaurant_id', 'feature_id']);

            $table->foreign('restaurant_id', 'restaurant_features_id_foreign')
                ->references('id')
                ->on('restaurants')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('feature_id', 'features_restaurant_id_foreign')
                ->references('id')
                ->on('restaurant_features')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('restaurant_restaurant_features', function (Blueprint $table) {
            $table->dropForeign('restaurant_features_id_foreign');
            $table->dropForeign('features_restaurant_id_foreign');
            $table->drop();
        });

        Schema::table('restaurant_features', function (Blueprint $table) {
            $table->drop();
        });
    }
}
