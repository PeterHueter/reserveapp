<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('reserv_datetime');
            $table->integer('people_count');
            $table->unsignedInteger('purpose_id');
            $table->timestamps();

            $table->index('purpose_id');

            $table->foreign('purpose_id', 'purpose_reservation_id_foreign')
                ->references('id')
                ->on('purposes')
                ->onDelete('restrict')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservations', function (Blueprint $table) {
            $table->dropForeign('purpose_reservation_id_foreign');
            $table->drop();
        });

    }
}
