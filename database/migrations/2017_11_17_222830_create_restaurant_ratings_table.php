<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_ratings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedTinyInteger('rating')->nullable();
            $table->text('comment')->nullable();
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('restaurant_id');
            $table->boolean('moderation')->default(true);
            $table->softDeletes();
            $table->timestamps();

            $table->index('user_id');
            $table->index('restaurant_id');

            $table->foreign('restaurant_id', 'rating_restaurant_id_foreign')
                ->references('id')
                ->on('restaurants')
                ->onDelete('restrict')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('restaurant_ratings', function (Blueprint $table) {
            $table->dropForeign('rating_restaurant_id_foreign');
            $table->drop();
        });
    }
}
