<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('reservations', function (Blueprint $table) {
            $table->unsignedInteger('restaurant_id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('manager_id')->nullable();
            $table->timestamp('confirmed_at')->nullable();
            $table->enum('status', ['await', 'confirmed', 'declined'])->default('await');

            $table->index('restaurant_id');
            $table->index('user_id');
            $table->index('manager_id');

            $table->foreign('restaurant_id', 'restaurant_reservations_id_foreign')
                ->references('id')
                ->on('restaurants')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('user_id', 'user_reservations_id_foreign')
                ->references('id')
                ->on('restaurants')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('manager_id', 'manager_reservations_id_foreign')
                ->references('id')
                ->on('restaurants')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('reservations', function (Blueprint $table) {
            $table->dropForeign('restaurant_reservations_id_foreign');
            $table->dropForeign('user_reservations_id_foreign');
            $table->dropForeign('manager_reservations_id_foreign');
            $table->dropColumn('restaurant_id');
            $table->dropColumn('user_id');
            $table->dropColumn('manager_id');
            $table->dropColumn('confirmed_at');
        });

    }
}