<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuisineTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuisine_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });


        Schema::create('restaurant_cuisine_types', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->unsignedInteger('restaurant_id');
            $table->unsignedInteger('cuisine_id');
            $table->primary(['restaurant_id', 'cuisine_id']);

            $table->foreign('restaurant_id', 'restaurant_cuisine_id_foreign')
                ->references('id')
                ->on('restaurants')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('cuisine_id', 'cuisine_restaurant_id_foreign')
                ->references('id')
                ->on('cuisine_types')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('restaurant_cuisine_types', function (Blueprint $table) {
            $table->dropForeign('restaurant_cuisine_id_foreign');
            $table->dropForeign('cuisine_restaurant_id_foreign');
            $table->drop();
        });

        Schema::table('cuisine_types', function (Blueprint $table) {
            $table->drop();
        });
    }
}
