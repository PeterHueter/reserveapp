<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManagerRestaurantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manager_restaurants', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->unsignedInteger('restaurant_id');
                $table->unsignedInteger('manager_id');
                $table->primary(['restaurant_id', 'manager_id']);

                $table->foreign('restaurant_id', 'restaurant_manager_id_foreign')
                    ->references('id')
                    ->on('restaurants')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

                $table->foreign('manager_id', 'manager_restaurant_id_foreign')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('manager_restaurants', function (Blueprint $table) {
            $table->dropForeign('restaurant_manager_id_foreign');
            $table->dropForeign('manager_restaurant_id_foreign');
            $table->drop();
        });
    }
}
