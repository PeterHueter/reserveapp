<?php

use Illuminate\Database\Seeder;
use App\Models\Restaurant;
use Illuminate\Support\Carbon;

class RestaurantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $restaurants = [
            [
                'name' => 'Пиццафабрика',
                'description' => '“ПиццаФабрика” - это сеть ресторанов и кафе с доставкой. Сегодня мы работаем в Вологде, Череповце, Ярославле, Рыбинске, Иваново, Костроме и Чебоксарах, а в ближайших планах - открытие филиалов компании в других городах. С 2011 года мы дарим своим клиентам возможность устроить маленький праздник для себя и своих близких!',
                'rating' => 90,
                'available' => true,
                'min_price' => 1000,
                'street' => 'ул. Зосимовская, д. 47',
                'lat' => 59.215620,
                'long' => 39.900829,
                'monday_to' => '10:00',
                'monday_from' => '01:00',
                'tuesday_to' => '10:00',
                'tuesday_from' => '01:00',
                'wednesday_to' => '10:00',
                'wednesday_from'=> '01:00',
                'thursday_to' => '10:00',
                'thursday_from' => '01:00',
                'friday_to' => '10:00',
                'friday_from' => '01:00',
                'saturday_to' => '10:00',
                'saturday_from' => '01:00',
                'sunday_to' => '10:00',
                'sunday_from' => '01:00',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Бульвар',
                'description' => 'На выбор всегда имеется богатый ассортимент блюд, рассчитанный на самого изысканного ценителя качественной кухни. А наши официанты порадуют Вас своим гостеприимством и не заставят долго томиться в ожидании заказа.',
                'rating' => 85,
                'available' => false,
                'min_price' => 1000,
                'street' => 'ул. Батюшкова, 35',
                'lat' => 59.215620,
                'long' => 39.900829,
                'monday_to' => '10:00',
                'monday_from' => '01:00',
                'tuesday_to' => '10:00',
                'tuesday_from' => '01:00',
                'wednesday_to' => '10:00',
                'wednesday_from'=> '01:00',
                'thursday_to' => '10:00',
                'thursday_from' => '01:00',
                'friday_to' => '10:00',
                'friday_from' => '01:00',
                'saturday_to' => '10:00',
                'saturday_from' => '01:00',
                'sunday_to' => '10:00',
                'sunday_from' => '01:00',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Парижанка',
                'description' => 'Уютный французский интерьер, большой выбор кофе, кофейные и шоколадные коктейли, французский горячий шоколад, прекрасная чайная коллекция, эксклюзивные кондитерские изделия, десерты из мороженого.',
                'rating' => 90,
                'available' => true,
                'min_price' => 1000,
                'street' => 'ул. Мира, д.1',
                'lat' => 59.215620,
                'long' => 39.900829,
                'monday_to' => '10:00',
                'monday_from' => '01:00',
                'tuesday_to' => '10:00',
                'tuesday_from' => '01:00',
                'wednesday_to' => '10:00',
                'wednesday_from'=> '01:00',
                'thursday_to' => '10:00',
                'thursday_from' => '01:00',
                'friday_to' => '10:00',
                'friday_from' => '01:00',
                'saturday_to' => '10:00',
                'saturday_from' => '01:00',
                'sunday_to' => '10:00',
                'sunday_from' => '01:00',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Тонга',
                'description' => 'Кафе в центре города.большой выбор кофе, кофейные и шоколадные коктейли, французский горячий шоколад, прекрасная чайная коллекция, эксклюзивные кондитерские изделия, десерты из мороженого.',
                'rating' => 90,
                'available' => true,
                'min_price' => 1000,
                'street' => 'ул. Каменный мост, д.2',
                'lat' => 59.215620,
                'long' => 39.900829,
                'monday_to' => '10:00',
                'monday_from' => '01:00',
                'tuesday_to' => '10:00',
                'tuesday_from' => '01:00',
                'wednesday_to' => '10:00',
                'wednesday_from'=> '01:00',
                'thursday_to' => '10:00',
                'thursday_from' => '01:00',
                'friday_to' => '10:00',
                'friday_from' => '01:00',
                'saturday_to' => '10:00',
                'saturday_from' => '01:00',
                'sunday_to' => '10:00',
                'sunday_from' => '01:00',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Оливер',
                'description' => 'Oliver развивающийся паб-бар Вологды концепция которого построена на симбиозе барной культуры, ресторанного обслуживания и рок-н-рольной атмосферы. Вкусная еда, уникальные интерьер и море рок музыки.',
                'rating' => 90,
                'available' => true,
                'min_price' => 1000,
                'street' => 'ул. Галкинская, д.16',
                'lat' => 59.215620,
                'long' => 39.900829,
                'monday_to' => '10:00',
                'monday_from' => '01:00',
                'tuesday_to' => '10:00',
                'tuesday_from' => '01:00',
                'wednesday_to' => '10:00',
                'wednesday_from'=> '01:00',
                'thursday_to' => '10:00',
                'thursday_from' => '01:00',
                'friday_to' => '10:00',
                'friday_from' => '01:00',
                'saturday_to' => '10:00',
                'saturday_from' => '01:00',
                'sunday_to' => '10:00',
                'sunday_from' => '01:00',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]
        ];

        $cuisines = [
            [1,2,6,8],
            [1,3,6,8,10],
            [1,3,6,10],
            [1,3,6],
            [1,3]
        ];

        $types = [
            [2,6],
            [1,4,5],
            [1,4],
            [1,5],
            [4,5],
        ];

        foreach ($restaurants as $index => $data) {
            $rest = Restaurant::create($data);
            $rest->cuisines()->sync($cuisines[$index]);
            $rest->types()->sync($types[$index]);
        }
    }
}
