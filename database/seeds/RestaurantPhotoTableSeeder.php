<?php

use Illuminate\Database\Seeder;
use App\Models\RestaurantPhoto;

class RestaurantPhotoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $photos = [
            // Пиццафабрика
            1 => [
                '/data/photos/1/image_59fc198467379.jpg',
                '/data/photos/1/DSC_0734.JPG',
                '/data/photos/1/2017-03-05.jpg',
                '/data/photos/1/2017-06-04.jpg',
                '/data/photos/1/2017-10-14.jpg',
            ],
            // бульвар
            2 => [
                '/data/photos/1/dsc_1093-1.jpeg',
                '/data/photos/1/dsc_1085-1.jpeg',
                '/data/photos/1/DSC_1088.jpg',
            ],
            // Парижанка
            3 => [
                '/data/photos/1/parizhanka_01.jpg'
            ],

            // Тонга
            4 => [
                '/data/photos/1/TONGA.jpeg'
            ],
            // Оливер
            5 => [
                '/data/photos/1/oliver.jpg'
            ],

        ];

        foreach ($photos as $restaurant_id => $links) {
            foreach ($links as $index => $link) {
                RestaurantPhoto::create([
                    'restaurant_id' => $restaurant_id,
                    'link' => $link,
                    'cover' => $index === 0,
                ]);
            }            
        }        
    }
}
