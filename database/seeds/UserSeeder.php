<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $password = \Illuminate\Support\Facades\Hash::make('12345');
        $users = [
            [
                'phone' => '71111111111',
                'name' => 'Иван Иванов',
                'email' => 'ivan@mail.ru',
                'password' => $password,
                'role' => 'client',
            ],
            [
                'phone' => '72222222222',
                'name' => 'Виктория Лебедева',
                'email' => 'viktor@yandex.ru',
                'password' => $password,
                'role' => 'manager',
            ],
            [
                'phone' => '73333333333',
                'name' => 'Максим Львович',
                'email' => 'lvovich@gmail.com',
                'password' => $password,
                'role' => 'admin',
            ]
        ];

        foreach($users as $user) {
            $newUser = User::create([
                'phone' => $user['phone'],
                'name' => $user['name'],
                'email' => $user['email'],
                'password' => $user['password'],
            ]);
            $newUser->attachRole($user['role']);
            $newUser->favorites()->attach($newUser->id);
            $newUser->restaurants()->attach(1);
        }
    }
}
