<?php

use Illuminate\Database\Seeder;
use App\Models\Collections\CuisineType;
use App\Models\Collections\RestaurantFeatures;
use App\Models\Collections\RestaurantType;
use App\Models\Collections\Purpose;

class CollectionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cuisines = [
            'Азиатская',
            'Европейская',
            'Русская',
            'Грузинская',
            'Молекулярная',
            'Итальянская',
            'Французская',
            'Фастфуд',
            'Суши',
            'Стейк-хаус',
            'Здоровая',
            'Вегитарианская',
        ];

        $features = [
            'Wi-Fi',
            'Бронирование',
            'Доставка',
            'Банкет',
            'Детская команата',
            'Телевизор',
            'Летняя веранда',
            'Кабинки со шторами',
        ];

        $types = [
            'Ресторан',
            'Кафе',
            'Булочная',
            'Бар',
            'Клуб',
            'Столовая',            
        ];

        $purposes = [
            'Семейный отдых',
            'Отдых с друзьями',
            'Деловая встреча',
            'Романтическое свидание',
            'Банкет',
        ];

        foreach($cuisines as $cuisine) {
            CuisineType::create(['name' => $cuisine,]);
        }

        foreach($features as $feature) {
            RestaurantFeatures::create(['name' => $feature]);
        }

        foreach($types as $type) {
            RestaurantType::create(['name' => $type,]);
        }

        foreach ($purposes as $purpose) {
            Purpose::create(['name' => $purpose]);
        }
    }
}
