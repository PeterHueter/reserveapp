<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CollectionsSeeder::class);
        $this->call(RestaurantSeeder::class);
        $this->call(RestaurantPhotoTableSeeder::class);
        $this->call(LaraTrustSeeder::class);
        $this->call(UserSeeder::class);
    }
}
