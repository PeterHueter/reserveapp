<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class LaraTrustSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // role => [permission,...]
        $roles = [
            [
                'name' => 'client',
                'display_name' => 'Пользователь',
                'description' => 'Пользователь сервиса',
                'permissions' => [],
            ],
            [
                'name' => 'manager',
                'display_name' => 'Менеджер',
                'description' => 'Менеджер ресторана',
                'permissions' => [],
            ],
            [
                'name' => 'admin',
                'display_name' => 'Администратор',
                'description' => 'Администратор системы',
                'permissions' => [],
            ],
        ];

        foreach ($roles as $role) {
            Role::create([
                'name' => $role['name'],
                'display_name' => $role['display_name'],
                'description' => $role['description'],
            ]);
        }
    }
}
