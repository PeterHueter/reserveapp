<!DOCTYPE html>
<html>

<head>
	<!-- Standard Meta -->
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

	<!-- Site Properties -->
	<title>Responsive Elements - Semantic</title>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.13/semantic.min.css">
</head>

<body>
    <div class="ui container">
        <div class="ui stackable grid">
            <div class="four wide column">
                <div class="ui vertical fluid menu">
                <a class="item" href="{{route('web.restaurants.index')}}">Мои рестораны</a>
                <a class="item" href="{{route('web.reservations.index')}}">Заявки на бронирование</a>
                </div>
            </div>

            <div class="twelve wide stretched column">
                <div class="ui segment">
                @yield('content')
                </div>
            </div>
        </div>    
    </div>
</body>

@stack('scripts')
</html>