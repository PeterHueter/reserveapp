// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App'
import router from './router'
import axios from 'axios'

Vue.use(BootstrapVue)

router.beforeEach((from, to, next) => {
  axios.get('/api/auth/me')
    .then((request) => {
      if (from.path === '/login') next('/')
      else next()
    })
    .catch((error) => {
      if (error.response.status === 401) {
        if (from.path !== '/login') {
            next({
                path: '/login',
                query: {redirect: from.fullPath}
            })
        }
      }
      next()
    })
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: {
    App
  }
})
