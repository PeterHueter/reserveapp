export default {
  items: [
    {
      name: 'Консоль',
      url: '/dashboard',
      icon: 'icon-speedometer',
    },
    {
      name: 'Рестораны',
      url: '/restaurants',
      icon: 'icon-list',
    },
    {
        name: 'Бронь',
        url: '/reservations',
        icon: 'icon-bell',
    },
  ]
}
