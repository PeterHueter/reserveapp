<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('auth')
    ->group(function ($router) {
        Route::post('login', 'AuthController@login');
        Route::post('logout', 'AuthController@logout');
        Route::post('refresh', 'AuthController@refresh');
        Route::get('me', 'AuthController@me');
    });

Route::prefix('v1')
    ->namespace('api\v1')
    ->group(function() {
        Route::apiResource('restaurants', RestaurantController::class);
        
        // ресторан
        Route::prefix('restaurants/{restaurant}')
            ->namespace('Restaurant')
            ->group(function() {
                Route::apiResource('rating', RatingController::class, ['only' => ['index', 'store']]);
                Route::post('reservation', 'ReservationController@store')->middleware('jwt.auth');
                Route::post('favorite', 'FavoriteController@favorite');
            });


        // роуты клиента
        Route::prefix('c')
            ->namespace('Client')
            ->middleware(['jwt.auth'/*, 'role:client'*/])
            ->group(function() {
                Route::apiResource('reservations', ReservationController::class, ['only' => ['index', 'show', 'destroy']]);
                Route::apiResource('favorites', FavoriteController::class, ['only' => ['index', 'destroy']]);
            });

        // роуты менеджера
        Route::prefix('m')
            ->namespace('Manager')
            ->middleware(['jwt.auth'/*, 'role:manager'*/])
            ->group(function() {
                Route::get('restaurants', 'RestaurantController@index');
                Route::get('reservations', 'ReservationController@index');

                Route::apiResource('restaurants/{restaurant}/reservations', Restaurants\ManagerRestaurantController::class, ['only' => ['index', 'destroy', 'show']]);
                Route::post('restaurants/{restaurant}/reservations/{id}/confirm', 'ReservationsController@confirm');
            });
    });