<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RestaurantRating extends Model
{
    use SoftDeletes;

    protected $table = 'restaurant_ratings';

    protected $fillable = [
        'rating',
        'comment'
    ];
}
