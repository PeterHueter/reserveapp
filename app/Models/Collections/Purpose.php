<?php

namespace App\Models\Collections;

use Illuminate\Database\Eloquent\Model;

class Purpose extends Model
{
    //
    protected $table = 'purposes';

    protected $fillable = ['name'];

    protected $hidden = ['created_at', 'updated_at'];
}
