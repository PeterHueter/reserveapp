<?php

namespace App\Models\Collections;

use Illuminate\Database\Eloquent\Model;

class RestaurantType extends Model
{

    protected $table = 'restaurant_types';

    protected $fillable = [ 'name' ];

    protected $hidden = [ 'created_at', 'updated_at', 'pivot'];
}
