<?php

namespace App\Models\Collections;

use Illuminate\Database\Eloquent\Model;

class CuisineType extends Model
{
    protected $table = 'cuisine_types';

    protected $fillable = [ 'name' ];

    protected $hidden = [ 'created_at', 'updated_at', 'pivot'];
}
