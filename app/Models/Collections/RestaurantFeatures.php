<?php

namespace App\Models\Collections;

use Illuminate\Database\Eloquent\Model;

class RestaurantFeatures extends Model
{
    protected $table = 'restaurant_features';

    protected $fillable = [ 'name' ];
}
