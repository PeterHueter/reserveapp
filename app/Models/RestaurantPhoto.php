<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class RestaurantPhoto extends Model
{
    protected $table = 'restaurant_photos';

    protected $hidden = [
        'id',
        'restaurant_id',
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'cover' => 'boolean'
    ];

    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class, 'restaurant_id');
    }

    public function getLinkAttribute($value)
    {
        return url($value);
    }
}
