<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Collections\CuisineType;
use Auth;

class Restaurant extends Model
{
    use SoftDeletes;

    protected $table = 'restaurants';

    protected $fillable = [
        'name',
        'rating',
        'available',
        'min_price',
        'street',
        'lat',
        'long',
        'monday_to',
        'monday_from',
        'tuesday_to',
        'tuesday_from',
        'wednesday_to',
        'wednesday_from',
        'thursday_to',
        'thursday_from',
        'friday_to',
        'friday_from',
        'saturday_to',
        'saturday_from',
        'sunday_to',
        'sunday_from',
    ];

    protected $with = [
        'photos',
        'cuisines',
        'types',
    ];

    protected $appends = [
        'favorite',
    ];

    protected $hidden = [
        'deleted_at',
        'created_at',
        'updated_at',        
    ];

    public function photos()
    {
        return $this->hasMany(RestaurantPhoto::class, 'restaurant_id')->orderBy('cover', 'DESC');
    }

    public function reservations()
    {
        return $this->hasMany(Reservation::class, 'restaurant_id');
    }

    public function types()
    {
        return $this->belongsToMany(Collections\RestaurantType::class, 'restaurant_restaurant_types', 'restaurant_id', 'type_id');
    }

    public function ratings()
    {
        return $this->hasMany(RestaurantRating::class, 'restaurant_id');
    }

    public function cuisines()
    {
        return $this->belongsToMany(CuisineType::class, 'restaurant_cuisine_types', 'restaurant_id', 'cuisine_id');
    }
    
    public function getFavoriteAttribute()
    {
        return Auth::check() && in_array($this->id, Auth::user()->favorites->pluck('id')->toArray());
    }
}
