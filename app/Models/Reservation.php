<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reservation extends Model
{
    protected $table = 'reservations';

    protected $fillable = [
        'reserv_datetime',
        'people_count',
        'purpose_id',
    ];

    protected $dates = [
        'reserv_datetime',
    ];

    public function purpose()
    {
        return $this->belongsTo(Collections\Purpose::class, 'purpose_id');
    }

    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class, 'restaurant_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
