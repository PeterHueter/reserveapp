<?php

namespace App\Services;

use \GuzzleHttp\Client;
use App\Models\Reservation;
use Illuminate\Support\Facades\Config;


class ReservationNotifier {
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function notify(Reservation $reservation)
    {
        $nconfig = Config::get('notifier');

        $this->client->post($nconfig['url'], [
            'form_params' => [
                'phone' => $reservation->user->phone,
                'people_count' => $reservation->people_count,
                'purpose' => $reservation->purpose,
                'reservse_datetime' => $reservation->reserv_datetime,
                'username' => 'k1ll9',
            ]
        ]);
    }
}