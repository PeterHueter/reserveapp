<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Laratrust;

/**
 * Запрос на резервацию столика клиентом
 */
class ReservationStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Laratrust::hasRole('client');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'reserv_datetime' => 'required|date',
            'people_count' => 'required|integer|between:1,20',
            'purpose_id' => 'required|integer|exists:purposes,id',            
        ];
    }
}
