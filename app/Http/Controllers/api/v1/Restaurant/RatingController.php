<?php

namespace App\Http\Controllers\api\v1\Restaurant;

use App\Http\Requests\RatingRequest;
use App\Http\Controllers\Controller;
use App\Models\Restaurant;
use App\Models\RestaurantRating;
use Illuminate\Support\Facades\Auth;


class RatingController extends Controller
{
    public function index(Restaurant $restaurant)
    {
        return $restaurant->ratings()->simplePaginate();
    }

    public function store(RatingRequest $request, Restaurant $restaurant)
    {
        $rating = new RestaurantRating($request->all());
        $rating->user_id = Auth::user()->id;
        $restaurant->ratings()->save($rating);
    }

    /*
    TODO: во вторую очередь
    public function update(Request $request, Rating $rating)
    {
        //
    }

    public function destroy(Rating $rating)
    {
        // TODO: проверить права
        //$restaurant->delete();
    }
    */

}
