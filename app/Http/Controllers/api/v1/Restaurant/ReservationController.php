<?php

namespace App\Http\Controllers\api\v1\Restaurant;

use App\Http\Controllers\ManagerNotifierController;
use App\Services\ReservationNotifier;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Restaurant;
use App\Http\Requests\ReservationStoreRequest;
use App\Models\Reservation;
use Auth;

class ReservationController extends Controller
{
    /**
     * Пользователь делает запрос на бронирование столика
     */
    public function store(ReservationStoreRequest $request, Restaurant $restaurant, ReservationNotifier $notifier)
    {
        $reservation = new Reservation($request->all());
        $reservation->user_id = Auth::user()->id;
        $reservation->status = 'await';
        $restaurant->reservations()->save($reservation);

        $notifier->notify($reservation);
    }
}
