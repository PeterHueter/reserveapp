<?php

namespace App\Http\Controllers\api\v1\Restaurant;

use App\Http\Requests\RatingRequest;
use App\Http\Controllers\Controller;
use App\Models\Restaurant;
use App\Models\RestaurantRating;
use Auth;


class FavoriteController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    public function favorite(Restaurant $restaurant)
    {
        Auth::user()->favorites()->save($restaurant);
    }
}
