<?php

namespace App\Http\Controllers\api\v1\Manager\Restaurants;

use App\Http\Requests\RatingRequest;
use App\Http\Controllers\Controller;
use App\Models\Restaurant;
use App\Models\User;
use App\Models\RestaurantRating;
use Illuminate\Support\Facades\Auth;


class ManagerController extends Controller
{
    public function index(Restaurant $restaurant)
    {
        $user_id = Auth::user()->id;
        $manager = User::find($user_id);
        return $manager->restaurants()->simplePaginate();
    }

    public function show(Restaurant $restaurant)
    {
        return $restaurant;
    }

}
