<?php
/**
 * Created by PhpStorm.
 * User: pathfinder
 * Date: 19.11.17
 * Time: 13:33
 */


namespace App\Http\Controllers\api\v1\Manager\Restaurants;

use App\Http\Controllers\Controller;
use App\Models\Reservation;




class ManagerRestaurantController extends Controller
{

    public function index()
    {
        return Reservation::simplePaginate();
    }


    public function show($id)
    {
        return Reservation::find($id);
    }

   /* public function destroy($id)
    {
        return Reservation::find($id)->delete();
    }*/

}
