<?php

namespace App\Http\Controllers\api\v1\Manager;

use App\Http\Controllers\Controller;
use App\Http\Requests\ConfirmReservationRequest;
use App\Models\Reservation;
use Auth;

class ReservationController extends Controller
{
    public function index()
    {
        $restaurantIds = Auth::user()
            ->restaurants()
            ->pluck('id')
            ->toArray();

        return Reservation::whereIn('restaurant_id', $restaurantIds)
            ->with('restaurant', 'purpose')
            ->orderBy('id', 'DESC')
            ->simplePaginate();
    }

    public function confirm(Reservation $reservation)
    {
        $reservation->status = 'confirmed';
        $reservation->save();
    }

}
