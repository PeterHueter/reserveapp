<?php

namespace App\Http\Controllers\api\v1\Manager;

use App\Http\Controllers\Controller;
use App\Http\Requests\ConfirmReservationRequest;
use App\Models\Reservation;
use Auth;

class RestaurantController extends Controller
{
    public function index()
    {
        return Auth::user()
            ->restaurants()
            ->simplePaginate();
    }
}
