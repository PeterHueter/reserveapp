<?php

namespace App\Http\Controllers\api\v1\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Reservation;
use App\Http\Requests\ClientOwnReservation;
use Auth;

class ReservationController extends Controller
{
    /**
     * Список заявок на бронирование пользователя приложения.
     * Поддерживается фильтр по активным подтвержденным и ожидающим,
     * по отменённым, по всем.
     */
    public function index(Request $request)
    {
        // TODO: filter
        return Auth::user()
            ->reservations()
            ->with('restaurant', 'purpose')
            ->orderBy('id', 'DESC')
            ->get();
    }
    
    /**
     * Информация по пробированию
     */
    public function show(ClientOwnReservation $request, Reservation $reservation)
    {
        return $reservation->load('restaurant', 'purpose');
    }
    
    /**
     * Отменить бронирование, можно отменить подтверждённое бронирование
     */
    public function destroy(ClientOwnReservation $request, Reservation $reservation)
    {
        $reservation->status ='declined';
        $reservation->save();
    }
}
