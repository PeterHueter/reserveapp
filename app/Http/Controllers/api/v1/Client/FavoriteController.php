<?php

namespace App\Http\Controllers\api\v1\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Reservation;
use App\Http\Requests\ClientOwnReservation;
use App\Models\Restaurant;
use Auth;

class FavoriteController extends Controller
{
    public function index()
    {
        return Auth::user()
            ->favorites()
            ->orderBy('id', 'DESC')
            ->simplePaginate();
    }

    public function destroy(Restaurant $restaurant)
    {
        Auth::user()
            ->favorites()
            ->detach($restaurant->id);
    }
}
