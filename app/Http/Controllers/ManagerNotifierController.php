<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\UserResource;
use App\Models\User;

class ManagerNotifierController extends Controller
{

    public function register(Request $request)
    {
        $user = User::where('telegram_key', $request->only('telegram_key'))->get();

        if ($user) {
            $telegramId = $request->only('telegram_id');
            $user->telegram_id = $telegramId;
            $user->save();
        }
    }

}
